# compat-tools

## 介绍

本项目是一个兼容工具集合，旨在为从其他异构数据库，迁移到 openGauss 之后的系统，创建必要的函数，以及系统视图的兼容。为后续的系统运维与应用改造提供便利。

脚本中带有版本控制，直接运行脚本时，会根据如下三种情况进行处理：

1. 若待创建对象在目标数据库中不存在，则直接进行创建
2. 若待创建对象版本高于目标数据库中的对象版本，则进行升级重建
3. 若待创建对象版本不高于目标数据库中的对象版本，则跳过创建



## 软件架构

### SQL文件：

- runMe.sql: 总执行脚本，执行创建所有的兼容性对象
- checkMe.sql: 总检查脚本，检查目标数据库中的兼容性对象是否需要升级
- flush_privileges.sql: 权限刷新脚本，将兼容性对象的使用权限授予全部用户
- uninstall.sql: 卸载脚本，用于移除 compat-tools 创建的一系列兼容性相关对象
- Oracle_Views.sql: Oracle数据库兼容性数据字典及视图
- Oracle_Functions.sql: Oracle数据库兼容性函数
- Oracle_Packages.sql: Oracle数据库管理包兼容
- MySQL_Views.sql: MySQL数据库兼容性数据字典及视图 //TODO
- MySQL_Functions.sql: MySQL数据库兼容性函数
- DB2_Functions.sql: DB2数据库兼容性函数


### Python 脚本：

- test.py: 自动化测试脚本，依赖于 docker 服务，能自动化检查脚本在各个 openGauss 版本中的执行情况（注：该脚本为测试脚本，在compat-tools每个版本发布之前已经测试完毕，最终用户无需使用该脚本）
- batch_executor.py: 多数据库批量执行调度脚本，可对同一个数据库实例中的多套库，同时执行对象创建或版本检查


## 使用说明

直接在 openGauss 数据库中执行sql文本。

> 注意： 以下所有操作需要在当前脚本所在的目录中执行


### 创建所有兼容性对象

创建所有兼容性对象，包括： 视图，函数，管理包等。

```shell
# 本地用户，默认库登录
gsql -f runMe.sql

# 详细创建语法，注意 test 用户权限（需要具有 sysadmin 权限，从3.0版本开始，仅支持初始用户进行本地创建）
gsql -h 127.0.0.1 -p 5432 -U test -d database_name -f runMe.sql
```


### 创建单一类兼容对象

以下以 Oracle 兼容性视图为例，其他兼容性对象脚本，也可以以此方式创建。

```shell
# 本地用户，默认库登录
gsql -f Oracle_Views.sql

# 详细创建语法，注意 test 用户权限（需要具有 sysadmin 权限，从3.0版本开始，仅支持初始用户进行本地创建）
gsql -h 127.0.0.1 -p 5432 -U test -d database_name -f Oracle_Views.sql
```


### 版本检查

检查目标库中对象是否需要升级。

```shell
# 本地用户，默认库登录
gsql -f checkMe.sql

# 详细创建语法，注意 test 用户权限（需要具有 sysadmin 权限，从3.0版本开始，仅支持初始用户进行本地创建）
gsql -h 127.0.0.1 -p 5432 -U test -d database_name -f checkMe.sql
```


### 卸载 Compat-tools

卸载由 Compat-tools 创建的一系列兼容性对象。

```shell
# 本地用户，默认库登录
gsql -f uninstall.sql

# 详细创建语法，注意 test 用户权限（需要具有 sysadmin 权限，从3.0版本开始，仅支持初始用户进行本地创建）
gsql -h 127.0.0.1 -p 5432 -U test -d database_name -f uninstall.sql
```


### 批量执行

默认情况下，执行 runMe.sql 或 checkMe.sql 脚本，都只能针对已连接的某一套数据库。

若想针对一个数据库实例中的多套数据库，那么可使用 batch_executor.py 脚本。

```shell
# 查看批量调度脚本支持的命令行选项
python3 batch_executor.py -h

# 批量对实例中所有数据库创建兼容性对象
python3 batch_executor.py -f runMe.sql

# 仅针对 postgres,hongye 库进行兼容性对象版本检查
python3 batch_executor.py -d 'postgres,hongye' -f checkMe.sql
```


### 功能测试

可在使用当前脚本工具时，可使用内置的 `test.py` 自动化测试脚本对 SQL 脚本进行功能测试：

注意： 该测试脚本需要在已安装 Docker 环境，且具备外网环境的服务器上执行。

```
python3 test.py
```

测试脚本执行完成后，会生成 `test_result.md` 结果文档，记录本次测试的结果，如果有失败的测试用例，也会输出详细的失败用例详情。

测试文档主要展示两个部分内容：

1. 测试汇总： 测试汇总中显示当前已测试的目标库版本，数据库兼容度列表，以及总体的测试结果。
2. 失败详情： 若有失败的测试用例，则会展示失败详情。



## 结果说明

脚本运行结束后，会自动输出以下两类内容：

1. 本次脚本中涉及到的兼容对象，以及对应的操作内容

   ```shell
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- =====================================================================
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- Compat Object List:
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- =====================================================================
   gsql:Oracle_Functions.sql:1035: NOTICE:     | type      | name                                             | version | language | operation           |
   gsql:Oracle_Functions.sql:1035: NOTICE:     |-----------|--------------------------------------------------|---------|----------|---------------------|
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | months_between(timestamptz,timestamptz)          | 2.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | sys_guid()                                       | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | aggregate | wm_concat(text)                                  | 1.0     | internal | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | nvl2(anyelement,anyelement,anyelement)           | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | trunc(timestamp,text)                            | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | trunc(timestamptz,text)                          | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | nanvl(numeric,numeric)                           | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | regexp_substr(text,text,int4)                    | 2.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | regexp_substr(text,text,int4,int4,text,int4)     | 2.0     | plpgsql  | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | regexp_instr(text,text,int4,int4,int4,text,int4) | 1.0     | plpgsql  | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | regexp_replace(text,text,text,int4,int4,text)    | 1.0     | plpgsql  | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | regexp_count(text,text,int4,text)                | 2.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | replace(text,text)                               | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | ora_hash(anyelement,int4,int4)                   | 1.0     | plpgsql  | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | show(text)                                       | 1.0     | sql      | Skip due to version |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | function  | show_parameter(text)                             | 1.0     | sql      | Skip due to version |
   ```

   

2. 本次脚本中内置的测试用例的运行汇总

   确认所有测试用例都是最新执行，并且没有类型为 `FAILED` 的测试用例即可。

   ```shell
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- =====================================================================
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- Function Test Result:
   gsql:Oracle_Functions.sql:1035: NOTICE:  -- =====================================================================
   gsql:Oracle_Functions.sql:1035: NOTICE:     | result_type | case_count | start_time                 | complete_time              |
   gsql:Oracle_Functions.sql:1035: NOTICE:     |-------------|------------|----------------------------|----------------------------|
   gsql:Oracle_Functions.sql:1035: NOTICE:     | PASSED      |        387 | 2021-11-25 09:42:20.016619 | 2021-11-25 09:42:52.127892 |
   gsql:Oracle_Functions.sql:1035: NOTICE:     | NULL        |          6 | 2021-11-25 09:42:20.016619 | 2021-11-25 09:42:20.016619 |
   ```

   

3. 数据库兼容模式为 A （默认建库即为 A 兼容模式）时，可能会有如下用例失败：

   以下用例失败的原因是 A 模式下，空字符串会被认为是 NULL（与 Oracle 中行为类似），导致用例失败，可忽略。

   ```shell
   gsql:runMe.sql:139: NOTICE:  -- =====================================================================
   gsql:runMe.sql:139: NOTICE:  -- Test Detail (Failed or Null):
   gsql:runMe.sql:139: NOTICE:  -- =====================================================================
   gsql:runMe.sql:139: NOTICE:     | test_expression                              | result          | expect          | complete_time       |
   gsql:runMe.sql:139: NOTICE:     |----------------------------------------------|-----------------|-----------------|---------------------|
   gsql:runMe.sql:139: NOTICE:     | find_in_set('','a,b,c,dx')                   |                 | = 0             | 2021-12-07 07:46:32 |
   gsql:runMe.sql:139: NOTICE:     | find_in_set('x','')                          |                 | = 0             | 2021-12-07 07:46:32 |
   gsql:runMe.sql:139: NOTICE:     | find_in_set('','a,b,c,,dx')                  |                 | = 4             | 2021-12-07 07:46:32 |
   gsql:runMe.sql:139: NOTICE:     | find_in_set('', string_to_array('a,b,c,d ... |                 | = 0             | 2021-12-07 07:46:32 |
   gsql:runMe.sql:139: NOTICE:     | find_in_set('x', string_to_array('', ','))   |                 | = 0             | 2021-12-07 07:46:32 |
   gsql:runMe.sql:139: NOTICE:     | find_in_set('', string_to_array('a,b,c,, ... |                 | = 4             | 2021-12-07 07:46:32 |
   gsql:runMe.sql:139: NOTICE:     | isnull('')::text                             |            true | = 'false'       | 2021-12-07 07:46:32 |
   ```

   

### 查询已创建的兼容对象及版本

```sql
select * from compat_tools.compat_version;
```



### 查询测试用例详情

```sql
select * from compat_tools.compat_testing;
```



## Oracle 兼容视图

Oracle_Views.sql 包含如下几类 Oracle 兼容性视图：



### 管理视图： ALL\_\*, DBA\_\*, USER\_\*

>  `ALL_` 视图当前与 `DBA_` 视图一致，并未做权限检验，后期版本将会考虑权限校验。

> 默认安装视图时，若内核字典中对象名为纯小写，则查询结果中转换为纯大写，大小写混合则不变。
可在安装视图完成后，通过执行以下命令来还原成内核字典中的大小写：

```bash
gsql -f Oracle_Views_Upper.sql -v UPPER="'N'"
```

>如果要切换为转换大写，则执行以下命令：

```bash
gsql -f Oracle_Views_Upper.sql -v UPPER="'Y'"
```

* [DBA,ALL,USER]\_CATALOG
* [DBA,ALL,USER]\_CONS\_COLUMNS
* [DBA,ALL,USER]\_CONSTRAINTS
* [DBA,ALL,USER]\_DATA\_FILES
* [DBA,ALL,USER]\_DEPENDENCIES
* [DBA,ALL,USER]\_IND\_COLUMNS
* [DBA,ALL,USER]\_IND\_EXPRESSIONS
* [DBA,ALL,USER]\_IND\_PARTITIONS
* [DBA,ALL,USER]\_IND\_STATISTICS
* [DBA,ALL,USER]\_INDEX\_USAGE
* [DBA,ALL,USER]\_INDEXES
* [DBA,ALL,USER]\_OBJECTS
* [DBA,ALL,USER]\_PART\_INDEXES
* [DBA,ALL,USER]\_PART\_TABLES
* [DBA,ALL,USER]\_PROCEDURES
* [DBA,ALL,USER]\_SEGMENTS
* [DBA,ALL,USER]\_SEQUENCES
* [DBA,ALL,USER]\_SOURCE
* [DBA,ALL,USER]\_SYNONYMS
* [DBA,ALL,USER]\_TAB\_COL\_STATISTICS
* [DBA,ALL,USER]\_TAB\_COLS
* [DBA,ALL,USER]\_TAB\_COLUMNS
* [DBA,ALL,USER]\_TAB\_COMMENTS
* [DBA,ALL,USER]\_COL\_COMMENTS
* [DBA,ALL,USER]\_TAB\_MODIFICATIONS
* [DBA,ALL,USER]\_TAB\_PARTITIONS
* [DBA,ALL,USER]\_TAB\_SUBPARTITIONS
* [DBA,ALL,USER]\_PART\_KEY\_COLUMNS
* [DBA,ALL,USER]\_SUBPART\_KEY\_COLUMNS
* [DBA,ALL,USER]\_TAB\_STATISTICS
* [DBA,ALL,USER]\_TABLES
* [DBA,ALL,USER]\_TABLESPACES
* [DBA,ALL,USER]\_TRIGGER\_COLS
* [DBA,ALL,USER]\_TRIGGERS
* [DBA,ALL,USER]\_TYPES
* [DBA,ALL,USER]\_VIEWS
* [DBA,ALL,USER]\_JOBS
* [DBA,ALL,USER]\_JOBS\_RUNNING
* [DBA,ALL,USER]\_MVIEWS
* [DBA,ALL,USER]\_MVIEW\_LOGS
* [DBA,ALL,USER]\_MVIEW\_COMMENTS
* [DBA,ALL,USER]\_USERS
* [DBA,ALL]\_DIRECTORIES
* DBA_ROLES
* [DBA,USER]\_ROLE\_PRIVS
* [DBA,ALL,USER]\_TAB\_PRIVS
* PRODUCT\_COMPONENT\_VERSION
* PLAN\_TABLE
* DBA\_SOURCE\_ALL
* NLS\_DATABASE\_PARAMETERS
* NLS\_INSTANCE\_PARAMETERS
* NLS\_SESSION\_PARAMETERS



### 其他短名称视图

* DICTIONARY
* DICT
* COLS
* IND
* OBJ
* TAB
* DUAL (For 2.0 and above)



### 动态性能视图： GV\\$, V\$

* [GV,V]\\$DATABASE  -- V\\$DATABASE 查看当前库， GV\$DATABASE 查看所有库
* [GV,V]$INSTANCE
* [GV,V]$DATAFILE
* [GV,V]$LOCK
* [GV,V]$PARAMETER
* [GV,V]$PARAMETER\_VALID\_VALUES
* [GV,V]$SESSION
* [GV,V]$SESSTAT
* [GV,V]$SPPARAMETER
* [GV,V]$SYSSTAT
* [GV,V]$TABLESPACE
* [GV,V]$VERSION
* [GV,V]$NLS\_PARAMETERS
* [GV,V]$NLS\_VALID\_VALUES
* [GV,V]$SYSTEM\_WAIT\_CLASS
* [GV,V]$SQL
* [GV,V]$OPTION
* [GV,V]$LICENSE
* [GV,V]$STATNAME
* [GV,V]$MYSTAT
* [GV,V]$LISTENER_NETWORK

### 权限统一查询视图

* DBA\_DETAIL\_PRIVILEGES
* DBA\_ALL\_PRIVILEGES
* DBA\_ALL\_PRIVILEGES\_SQL



## Oracle 兼容函数

### 字符函数

- regexp_substr(text,text,int4)
- regexp_substr(text,text,int4,int4,text,int4)    -- 注意： 最后一个参数 p_subexpr 不支持
- regexp_instr(text,text,int4,int4,text,int4)
- regexp_replace(text,text,text,int4,int4,text)
- regexp_count(text,text,int4,text)
- regexp_count(text,text,int4)
- regexp_count(text,text)
- replace(text,text)
- instrb(text,text,int4,int4)
- soundex(text)
- asciistr(text)
- unistr(text)
- convert(text,text,text)



### 时间函数

- months_between(timestamp, timestamp)
- trunc(timestamp, text)
- trunc(timestamptz, text)
- round(timestamptz,text)
- tz_offset(text)
- numtoyminterval(numeric,text)
- systimestamp()
- sys_extract_utc(timestamptz)
- new_time(timestamp,text,text)

### 数字函数

- nanvl(numeric,numeric)
- bitor(int8,int8)
- bitxor(int8,int8)
- bin_to_num(int4[])
- sinh(numeric)
- cosh(numeric)
- tanh(numeric)
- remainder(numeric,numeric)
- round_ties_to_even(numeric,int4)

### 聚合函数

- wm_concat(text)
- bit_and_agg(numeric)
- bit_or_agg(numeric)
- bit_xor_agg(numeric)
- any_value(anyelement)
- kurtosis_pop(numeric)
- kurtosis_samp(numeric)
- skewness_pop(numeric)
- skewness_samp(numeric)



### NULL 值相关函数

- nullif(anyelement, anyelement)
- nvl2(anyelement, anyelement, anyelement)
- nvl2(text, text, text)
- nvl2(numeric, numeric, numeric)
- nvl2(timestamptz, timestamptz, timestamptz)
- lnnvl(bool)


### 其他函数

- sys_guid()
- ora_hash(anyelement,bigint,bigint)
- show(text)    -- 提供类似 Oracle 中 show xxx 查看相关参数的功能
- show_parameter(text)    -- 等同于 show(text)
- dump(anyelement,numeric,int4,int4)
- vsize(any)
- to_blob(raw)
- nls_charset_name(int4)
- nls_charset_id(text)
- sys_context(text,text,int4)
- userenv(text)
- empty_clob()


## Oracle 管理包

### DBMS_METADATA

* get_ddl(object_type, object_name, schema_name)



### DBMS_OUTPUT

* enable(int4)
* disable()
* get_line(text, int4)
* get_lines(text[], int4)
* new_line()
* put(text)
* put_line(text)



### DBMS_RANDOM

- initialize(int4)    -- deprecated in Oracle
- normal()
- random()    -- deprecated in Oracle
- seed(int4)
- seed(text)
- string(text,int4)
- terminate()    -- deprecated in Oracle
- value()
- value(numeric,numeric)



### DBMS_JOB

- broken(int8,bool,timestamp)
- change(int8,text,timestamp,text,int4,bool)
- instance(int8,int4,bool)
- interval(int8,text)
- next_date(int8,timestamp)
- remove(int8)
- run(int8,bool)
- submit(int8,text,timestamp,text,bool,int4,bool)
- user_export(int8,text)
- what(int8,text)



### DBMS_LOCK

- sleep(float8)
- allocate_unique(text,text,int4)
- request(int8,int4,int4,bool)
- request(text,int4,int4,bool)
- release(int8)
- release(text)


### dbms_application_info

- read_client_info(text)
- read_module(text,text)
- set_client_info(text)
- set_module(text,text)



### dbms_obfuscation_toolkit

- md5(text)



### dbms_utility

- canonicalize(text)
- comma_to_table(text,int4,text[])
- table_to_comma(text[],int4,text)
- db_version(text,text)
- exec_ddl_statement(text)

### utl_url     

- escape(text,bool,text)
- unescape(text,text)



### utl_encode               
- base64_encode(raw)
- base64_decode(raw)
- text_encode(text,text,int4)
- text_decode(text,text,int4)
- quoted_printable_encode(raw)
- quoted_printable_decode(raw)
- mimeheader_encode(text,text,int4)
- mimeheader_decode(text)
- uuencode(raw,int1,text,text)
- uudecode(raw)



### utl_raw                  
- cast_to_varchar2
- cast_to_raw(text)
- concat(raw,raw,raw,raw,raw,raw,raw,raw,raw,raw,raw,raw)
- length(raw)
- substr(raw,int4,int4)
- transliterate(raw,raw,raw,raw)
- translate(raw,raw,raw)
- copies(raw,int8)
- overlay(raw,raw,int4,int4,raw)
- xrange(raw,raw)
- reverse(raw)
- compare(raw,raw,raw)
- convert(raw,text,text)
- bit_and(raw,raw)
- bit_or(raw,raw)
- bit_xor(raw,raw)
- bit_complement(raw)
- cast_to_number(raw)
- cast_from_number(numeric)
- cast_to_binary_integer(raw,int1)
- cast_from_binary_integer(int4,int1)

### dbms_lob
- getlength(blob)
- getlength(clob)
- open(blob,int)
- open(clob,int)
- freetemporary(blob)
- freetemporary(clob)
- substr(blob,int4,int4)
- substr(clob,int4,int4)
- instr(blob,raw,int4,int4)
- instr(clob,text,int4,int4)
- createtemporary(blob,bool,int4)
- createtemporary(clob,bool,int4)
- close(blob)
- close(clob)
- append(blob,blob)
- append(clob,clob)
- compare(blob,blob,int4,int4,int4)
- compare(clob,clob,int4,int4,int4)
- copy(blob,blob,int4,int4,int4)
- copy(clob,clob,int4,int4,int4)
- erase(blob,int4,int4)
- erase(clob,int4,int4)
- read(blob,int4,int4,raw)
- read(clob,int4,int4,text)
- trim(blob,int4)
- trim(clob,int4)
- write(blob,int4,int4,raw)
- write(clob,int4,int4,text)
- writeappend(blob,int4,raw)
- writeappend(clob,int4,text)

### dbms_snapshot
- refresh(text,text)

### utl_match
- edit_distance(text,text)
- edit_distance_similarity(text,text)

## Oracle 兼容类型
- pls_integer

## MySQL 兼容函数

### NULL 相关函数

> 注意： ifnull 效果等同于通用函数 coalesce，在条件允许的情况下，建议修改 SQL 使用 coalesce 函数，该函数几乎在各个数据库中均支持

- ifnull(text,text)
- ifnull(numeric,numeric)
- ifnull(timestamp,timestamp)
- ifnull(timestamptz,timestamptz)
- isnull(text)    -- 注意： openGauss 中如果创建数据库时，使用默认的 A 兼容模式，那么 '' 空字符串也会被识别为 NULL 值
- isnull(numeric)
- isnull(timestamptz)



### 条件控制函数

- if(bool,bool,bool)
- if(bool,text,text)
- if(bool,numeric,numeric)
- if(bool,timestamptz,timestamptz)



### 字符函数

- find_in_set(text,text)
- find_in_set(text,text[])
- field(text,text[])
- elt(int4,text[])
- strcmp(text,text)
- insert(text,int8,int8,text)
- lcase(text)
- ucase(text)
- space(int4)
- mid(text,int8,int8)
- locate(text,text,int4)
- to_base64(text)
- from_base64(text)



### 数字函数

- field(numeric,numeric[])
- log10(numeric)
- log10(float8)
- rand(int4)



### 时间函数

- unix_timestamp(timestamp)
- unix_timestamp(timestamptz)
- from_unixtime(int8)
- from_unixtime(numeric)
- from_unixtime(numeric,text)
- to_days(timestamp)
- to_days(timestamptz)
- to_days(text)
- to_days(text,text)
- to_days(int8)
- to_seconds(timestamp)
- to_seconds(timestamptz)
- timediff(timestamptz,timestamptz)
- time_to_sec(time)
- sec_to_time(int4)
- date_format(timestamp, text)
- date_format(timestamptz, text)
- timestampdiff(text,timestamptz,timestamptz)    -- 注意： 使用必须带 pg_catalog. 前缀Schema，第一个单位参数必须使用单引号包裹
- str_to_date(text,text)    -- 由于 MySQL 与 openGauss 中时间类型的差异，本兼容函数并非完全一致性的兼容，详细请查阅对应函数尾部的用例描述部分
- curdate()
- yearweek(timestamp,int4)  --由于opgenGauss年的周起始时间是用的iso标准，对应mysql的第二个参数为3，因此本函数并非完全兼容
- yearweek(timestamptz,int4)
- yearweek(text,int4)
- date_add(timestamp,interval) --第二个参数的数字部分需要加上单引号
- date_add(timestamptz,interval)


### 其他函数

- uuid()    -- 提供基于 随机值+时间戳 的哈希计算 uuid
- uuid_to_bin(uuid, int4)
- bin_to_uuid(bytea, int4)
- conv(text, int4, int4)    -- 目前只支持 2, 10, 16 进制之间的相互转换
- json_extract(json,text)
- json_extract(json,text[])


## DB2 兼容函数

### NULL 相关函数

> 注意： value 效果等同于通用函数 coalesce，在条件允许的情况下，建议修改 SQL 使用 coalesce 函数，该函数几乎在各个数据库中均支持

- value(text,text)
- value(numeric,numeric)
- value(timestamp,timestamp)
- value(timestamptz,timestamptz)



### 字符函数

- posstr(text,text)
- locate_in_string(text,text,int4,int4,text)
- regexp_match_count(text,text,int4,text,text)



### 时间函数

- year (text)
- year (timestamptz)
- year (interval)
- month (text)
- month (timestamptz)
- month (interval)
- quarter (timestamptz)
- week (timestamptz)
- day (text)
- day (timestamptz)
- day (interval)
- hour (text)
- hour (timestamptz)
- hour (interval)
- minute (text)
- minute (timestamptz)
- minute (interval)
- second (text)
- second (timestamptz)
- second (interval)
- days (timestamptz)
- dayofyear (timestamptz)
- dayofweek (timestamptz)
- dayofweek_iso (timestamptz)
- dayname (timestamptz)
- monthname (timestamptz)
- midnight_seconds (timestamptz)
- next_day (timestamptz,text,text)
- next_month (timestamptz)
- next_quarter (timestamptz)
- next_week (timestamptz)
- next_year (timestamptz)
- last_day (timestamptz)
- first_day (timestamptz)
- this_month (timestamptz)
- this_quarter (timestamptz)
- this_week (timestamptz)
- this_year (timestamptz)
- days_between (timestamptz,timestamptz)
- years_between (timestamptz,timestamptz)
- ymd_between (timestamptz,timestamptz)



## Roadmap

