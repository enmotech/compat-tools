set behavior_compat_options = '';

create or replace function compat_tools.f_upper_name( name text,convert_ind text default :UPPER) 
returns text immutable as
$$ select case when convert_ind='Y' then upper(name) else name end; 
$$ language sql;

reset behavior_compat_options;